
package com.example.generala

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import kotlin.random.Random
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val texto: TextView = findViewById(R.id.textView7)
        val imagen1: ImageView = findViewById(R.id.imageView)
        val imagen2: ImageView = findViewById(R.id.imageView2)
        val imagen3: ImageView = findViewById(R.id.imageView3)
        val imagen4: ImageView = findViewById(R.id.imageView4)
        val imagen5: ImageView = findViewById(R.id.imageView5)
        val apariciones: HashMap<Int, Int> = HashMap()
        val dados = IntArray(5)
        var ban=0
        fun rand(start: Int, end: Int): Int {
            //require(start <= end) { "Illegal Argument" }
            return Random(System.nanoTime()).nextInt(start, end )
        }
        val botonPlay: Button = findViewById(R.id.button3)
        botonPlay.setOnClickListener{
            //carga de dados
            for(i in dados.indices){
                var valorRandom = rand(1,6)
                dados[i]= valorRandom.toInt()
                if (i == 0){
                    imagen1.setImageResource(getImagen(valorRandom))
                }
                if (i == 1){
                    imagen2.setImageResource(getImagen(valorRandom))
                }
                if (i == 2){
                    imagen3.setImageResource(getImagen(valorRandom))
                }
                if (i == 3){
                    imagen4.setImageResource(getImagen(valorRandom))
                }
                if (i == 4){
                    imagen5.setImageResource(getImagen(valorRandom))
                }
                if (i == 5){
                    imagen5.setImageResource(getImagen(valorRandom))
                }

            }
            //calculo de Apariciones
            for (i in dados) {
                var cont = apariciones[i]
                if (cont == null) cont = 0
                apariciones[i] = cont + 1
            }
            //funcion escalera
            fun escalera(): String {
                var resultado =""
                if (apariciones.containsKey(1) && apariciones.containsKey(2) && apariciones.containsKey(3) && apariciones.containsKey(4) && apariciones.containsKey(5) || apariciones.containsKey(2) && apariciones.containsKey(3) && apariciones.containsKey(4) && apariciones.containsKey(5) && apariciones.containsKey(6) || apariciones.containsKey(3) && apariciones.containsKey(4) && apariciones.containsKey(5) && apariciones.containsKey(6) && apariciones.containsKey(1)){
                    resultado = "Obtuviste una Escalera"
                    ban=1
                    texto.text= resultado
                }
                return resultado
            }
            //funcion generala
            fun generalaa():String{
                var resultado=""
                if (apariciones.containsValue(5)) {
                    resultado="Obtuviste una Generala"
                    ban=1
                    texto.text= resultado
                }
                return resultado
            }
            //funcion poker
            fun poker(): String{
                var resultado=""
                if (apariciones.containsValue(4)) {
                    resultado="Obtuviste Poker"
                    ban=1
                    texto.text= resultado
                }
                return resultado
            }
            //funcion FULL
            fun full(): String{
                var resultado=""
                if (apariciones.containsValue(3) && apariciones.containsValue(2)) {
                    resultado="Obtuviste FULL"
                    ban=1
                    texto.text= resultado
                }
                return resultado
            }

            escalera()
            generalaa()
            poker()
            full()
            if(ban==0){
                val resultado = "No salió nada"
                texto.text= resultado
            }
            println(dados)
            println(apariciones)
            apariciones.clear()
            ban=0
        }
    }

    private fun getImagen(valor: Int): Int {
        var aux=0
        if (valor == 1) {
            aux=R.drawable.dice_1
        }
        if (valor == 2) {
            aux= R.drawable.dice_2
        }
        if (valor == 3) {
            aux= R.drawable.dice_3
        }
        if (valor == 4) {
            aux= R.drawable.dice_4
        }
        if (valor == 5) {
            aux= R.drawable.dice_5
        }
        return aux
    }


}
